/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// Optional: Declare as a Spring configuration class
@Configuration
public class SpringDomainConfig {

	// Optional: Declare as a bean definition
	@Bean
	public Integer maxSearchResults () {
		return Math.max(1, (int) (Math.random()*10));
	}

}