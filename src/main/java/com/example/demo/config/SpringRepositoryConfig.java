/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.persistence.InMemoryItemRepository;
import com.example.demo.persistence.ItemRepository;

// Declare as a configuration class
@Configuration
public class SpringRepositoryConfig {
	
	// Optional: Inject this value
	@Inject
	private Integer maxSearchResults;

	// Declare the item repository bean
	@Bean
	public ItemRepository itemRepository() {
		// Pass a CatalogData to the repository
		InMemoryItemRepository rep = new InMemoryItemRepository();
		// Optional: Use our injected value to configure the repository
		rep.setMaxSearchResults(maxSearchResults);
		return rep;
	}
}