/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

// Declare as a Spring configuration class
// and import other configuration classes
@Configuration
@Import({SpringDomainConfig.class, SpringRepositoryConfig.class, SpringServicesConfig.class})  // Include Optional SpringDomainConfig
public class SpringConfig {}